CONTENTS OF THIS FILE
____________________
   
 * Introduction  
 * Description
 * Installation
 * Configuration

INTRODUCTION
____________________

Live chat widget for you website.

DESCRIPTION
____________________

The Chatra module which provides a plugin for "Chatra" which is a
Modern Live Chat Software for Boost conversions, solve issues,in real time.

Live chat allows you to answer questions and alleviate concerns,
unstucking visitors and helping them place an order.
Use it to collect actionable feedback to improve your service:
identify problems, capture leads and know your customers.

INSTALLATION
____________________

To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Visit admin/modules and enable the "Chatra" module.

CONFIGURATION
____________________

To configure this module do the following:

1. Sign up for a free Chatra account at https://app.chatra.io

2. Go to Configuration -> Chatra Settings (admin/config/system/chatra).

3. In order to activate your live chat widget paste this snippet
into the box provided in the configuration page

4. Code Snippet :

<script>
    ChatraID = '[ChatraID]';
    (function(d, w, c) {
        var n = d.getElementsByTagName('script')[0],
            s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = (d.location.protocol === 'https:' ? 'https:': 'http:')
            + '//chat.chatra.io/chatra.js';
        n.parentNode.insertBefore(s, n);
    })(document, window, 'Chatra');
</script>
