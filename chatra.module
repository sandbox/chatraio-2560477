<?php

/**
 * @file
 * Use to allow group chats.
 */

/**
 * Implements hook_menu().
 */
function chatra_menu() {
  $items['admin/config/system/chatra'] = array(
    'title' => 'Chatra Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('chatra_settings'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Provides interface for chatra live chat setup.
 */
function chatra_settings($form, &$form_state) {
  $form = array();
  $form['title'] = array(
    '#markup' => '<h3>' . t('Chatra Live Chat Setup') . '</h3>',
  );

  $yes_markup = t('Sign up for a free Chatra account at <a href="!url">app.chatra.io</a>,!line_break
 then copy and paste Widget code from Set up & customize section into the form below:',
    array(
      '!url' => 'http://app.chatra.io/?utm_source=drupal',
      '!line_break' => '<br />',
    )
  );

  $no_markup = t('Seems like everything is OK!!line_break
Check your <a href="!website">website</a> to see if the live chat widget is present.!line_break
Log in to your <a href="!url">Chatra Dashboard</a> to chat with you website visitors and manage preferences.',
    array(
      '!line_break' => '<br />',
      '!website' => 'http://app.chatra.io',
      '!url' => 'http://app.chatra.io?utm_source=drupal',
    )
  );

  $form['info'] = array(
    '#markup' => (variable_get('chatra_script',
        '') == '') ? $yes_markup : $no_markup,
  );
  $form['chatra_script'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('chatra_script', ''),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_js_alter().
 */
function chatra_js_alter(&$js) {
  if (path_is_admin(current_path())) {
    return;
  }

  $token = variable_get('chatra_script', '');
  if (empty($token)) {
    return;
  }

  $token = str_replace(array('<script>', '</script>'), '', $token);

  $js[] = array(
    'type' => 'inline',
    'scope' => 'footer',
    'weight' => 5,
    'group' => JS_DEFAULT,
    'every_page' => TRUE,
    'requires_jquery' => FALSE,
    'data' => $token,
    'defer' => FALSE,
  );
}
